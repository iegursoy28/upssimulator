using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Upsp;

namespace UpsSimulator
{
    class VirtualUpsSimulator
    {
        private List<Thread> simulators = new List<Thread>();

        private static string Host;
        private static int Port;

        public VirtualUpsSimulator(int cout, string host, int port)
        {
            Host = host;
            Port = port;

            for (int i = 0; i < cout; i++)
            {
                Thread t = new Thread(VirtualUpsSimulator.PushUpsValueAsync);
                simulators.Add(t);
                t.Start("123456789-" + i);
            }

            foreach (var item in simulators)
                item.Join();
        }

        public void stop()
        {
            foreach (var item in simulators)
                item.Interrupt();

            System.Console.WriteLine("Streaming stop.");
        }

        public static async void PushUpsValueAsync(object serial)
        {
            var channel = new Channel(Host + ":" + Port, ChannelCredentials.Insecure);
            var client = new UpsValueStreamService.UpsValueStreamServiceClient(channel);

            using (var call = client.PushStreamUpsValues())
            {
                var c = new CoordinateQ()
                {
                    Latitude = SimpleUpsValues.GetRandomNumber(37.0078, 41.7594),
                    Longtitude = SimpleUpsValues.GetRandomNumber(27.2509, 43.7641)
                };

                System.Console.WriteLine(serial + " Push streaming start.");

                for (int i = 0; ; i++)
                {
                    UpsQ u = new UpsQ()
                    {
                        Info = new UpsInfo() { UpsSerial = (string)serial, Coordinate = c },
                        UpsMode = new UpsMode() { Mode = UpsMode.Types.RunningMode.OnlineMode }
                    };
                    u.Value.Add(SimpleUpsValues.newInstance((string)serial));

                    try { await call.RequestStream.WriteAsync(u); }
                    catch (System.Exception) { break; }
                    System.Console.WriteLine(serial + " Push stream: " + i.ToString());
                    Thread.Sleep(1000);
                }

                System.Console.WriteLine(serial + " Push streaming stop.");
                await call.RequestStream.CompleteAsync();
            }
        }
    }
}