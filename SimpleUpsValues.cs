using System;
using Upsp;
using Grpc.Core;

namespace UpsSimulator
{
    class SimpleUpsValues
    {
        public static double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        public static OutputValues GetUpsOutput()
        {
            OutputValues val = new OutputValues()
            {
                VoltageR = GetRandomNumber(219, 221),
                VoltageS = GetRandomNumber(219, 221),
                VoltageT = GetRandomNumber(219, 221),
                LoadPercentR = GetRandomNumber(50, 55),
                LoadPercentS = GetRandomNumber(50, 55),
                LoadPercentT = GetRandomNumber(50, 55),
                Frequency = GetRandomNumber(49, 51)
            };

            return val;
        }

        public static BatteryDcValues GetUpsBatteryDc()
        {
            BatteryDcValues val = new BatteryDcValues()
            {
                VoltagePositive = GetRandomNumber(400, 405),
                VoltageNegative = GetRandomNumber(400, 405),
                CurrentPositive = GetRandomNumber(10, 11),
                CurrentNegative = GetRandomNumber(10, 12),
                BackupTime = 600,
                Temperature = GetRandomNumber(58, 60),
                Percent = GetRandomNumber(70, 73)
            };

            return val;
        }

        public static BypassValues GetUpsBypass()
        {
            BypassValues val = new BypassValues()
            {
                VoltageR = GetRandomNumber(219, 221),
                VoltageS = GetRandomNumber(219, 221),
                VoltageT = GetRandomNumber(219, 221),
                Frequency = GetRandomNumber(49, 51)
            };

            return val;
        }

        public static InputValues GetUpsInput()
        {
            InputValues val = new InputValues()
            {
                VoltageR = GetRandomNumber(219, 221),
                VoltageS = GetRandomNumber(219, 221),
                VoltageT = GetRandomNumber(219, 221),
                Frequency = GetRandomNumber(49, 52)
            };

            return val;
        }

        public static UpsAbout GetUpsAbout(string serial)
        {
            UpsAbout val = new UpsAbout()
            {
                Name = "ETR 3/3 10 kVA",
                Serial = serial,
                Output = "220V/330V 50Hz",
                FirmwareVersion = "v3.5.6 - v3.5.8",
                InverterVersion = "unknown",
            };

            return val;
        }

        public static UpsStatus GetUpsStatus()
        {
            UpsStatus val = new UpsStatus()
            {
                DcAndRectifierStatusRecRotError = 1,
                DcAndRectifierStatusLowBatteryShutdown = 16,
                DcAndRectifierStatusLowBattery = 16,
                DcAndRectifierStatusInAndOut = 0,
                DcAndRectifierStatusBatteryStatus = 5,
                DcAndRectifierStatusChargeStatus = 16,
                DcAndRectifierStatusRecOperating = 14,
                UpsStatusBypassFrequencyFail = 16,
                UpsStatusManualBypassBreaker = 9,
                UpsStatusAcStatus = 10,
                UpsStatusStaticSwitchMode = 12,
                UpsStatusInverterOperating = 16,
                FaultStatusEmergencyStop = 16,
                FaultStatusHighDcShutdown = 16,
                FaultStatusBypassBreaker = 16,
                FaultStatusOverLoad = 16,
                FaultStatusInverterOutputFail = 16,
                FaultStatusOverTemperature = 16,
                FaultStatusShortCircuit = 16
            };

            return val;
        }

        public static UpsValues newInstance(string serial)
        {
            return new UpsValues()
            {
                OutputValues = SimpleUpsValues.GetUpsOutput(),
                BatteryDcValues = SimpleUpsValues.GetUpsBatteryDc(),
                BypassValues = SimpleUpsValues.GetUpsBypass(),
                InputValues = SimpleUpsValues.GetUpsInput(),
                UpsAbout = SimpleUpsValues.GetUpsAbout(serial),
                UpsStatus = SimpleUpsValues.GetUpsStatus(),
                Time = DateTimeOffset.UtcNow.ToUnixTimeSeconds()
            };
        }
    }
}