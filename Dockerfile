FROM microsoft/dotnet:2.1-sdk

COPY . /server/
RUN apt update && apt install make
RUN cd /server/ && make

