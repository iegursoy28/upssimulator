using System;
using System.Threading.Tasks;
using Grpc.Core;
using Upsp;
using static Upsp.UpsStateService;

namespace UpsSimulator
{
    class UpsClient : IDisposable
    {
        private string _host;
        private int _port;

        private Channel channel;
        private UpsStateServiceClient client;

        public UpsClient(string host, int port)
        {
            _host = host;
            _port = port;

            try
            {
                channel = new Channel(_host + ":" + _port, ChannelCredentials.Insecure);
                client = new UpsStateService.UpsStateServiceClient(channel);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
                Dispose();
            }
        }

        public void Dispose()
        {
            if (channel != null)
            {
                try { channel.ShutdownAsync(); } catch { }
                System.Console.WriteLine("Shutdown client.");
            }
        }

        /*
            rpc GetUpsList(UpsListQuery) returns (UpsInfoList) {}
            rpc GetUpsValues(UpsValueQuery) returns (UpsValueList) {}
            rpc GetUpsMode(UpsInfo) returns (UpsMode) {}
        */

        public async Task<UpsInfoList> GetUpsListAsync()
        {
            var q = new UpsListQuery() { GetActive = false };
            var res = await client.GetUpsListAsync(q, deadline: DateTime.UtcNow.AddSeconds(5));
            foreach (var l in res.UpsList)
                System.Console.WriteLine(String.Format("name: {0}, serial: {1}, coordinate: [{2}, {3}]",
                                l.UpsName, l.UpsSerial, l.Coordinate.Latitude, l.Coordinate.Longtitude));
            return res;
        }

        public async Task<UpsInfoList> GetActiveUpsListAsync()
        {
            UpsListQuery q = new UpsListQuery() { GetActive = true };
            var res = await client.GetUpsListAsync(q, deadline: DateTime.UtcNow.AddSeconds(5));
            foreach (var l in res.UpsList)
                System.Console.WriteLine(String.Format("name: {0}, serial: {1}, coordinate: [{2}, {3}]",
                                l.UpsName, l.UpsSerial, l.Coordinate.Latitude, l.Coordinate.Longtitude));
            return res;
        }

        public async Task<UpsValueList> GetUpsValuesAsync(string serail)
        {
            var i = new UpsInfo() { UpsSerial = serail };
            var q = new UpsValueQuery() { Info = i, Limit = 50 };
            var res = await client.GetUpsValuesAsync(q, deadline: DateTime.UtcNow.AddSeconds(5));
            foreach (var v in res.ValuesList)
                System.Console.WriteLine(String.Format("serial: {0} time: {1}, val: {2}",
                                serail, FromUnixTime(v.Time / 1000).ToString("yyyy-MM-dd HH:mm:ss"), v.ToString()));
            return res;
        }

        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public static DateTime FromUnixTime(long unixTime)
        {
            return epoch.AddSeconds(unixTime);
        }
    }
}