using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using CommandLine;

namespace UpsSimulator
{
    class Program
    {
        public class Options
        {
            [Option('h', "host", Required = true, HelpText = "Set server host.")]
            public string Host { get; set; }

            [Option('p', "port", Required = false, Default = 2222, HelpText = "Set server port.")]
            public int Port { get; set; }

            [Option('c', "count", Required = false, Default = 10, HelpText = "Virtual simulator count.")]
            public int Count { get; set; }

            [Option('s', "simulator", Required = false, Default = false, HelpText = "Running mode is sinulator.")]
            public bool IsSimulator { get; set; }
        }

        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
            .WithParsed<Options>(o => RunWithOpts(o))
            .WithNotParsed<Options>(e => ErrorHandle(e));
        }

        static void ErrorHandle(IEnumerable<Error> e)
        {
            foreach (var item in e)
                System.Console.WriteLine(item.StopsProcessing);
        }

        static void RunWithOpts(Options o)
        {
            if (o.IsSimulator)
            {
                VirtualUpsSimulator vus = new VirtualUpsSimulator(o.Count, o.Host, o.Port);
                System.Console.WriteLine(String.Format("Start {0} simulators\nEnd point: {1}:{2}", o.Count, o.Host, o.Port));
                Console.WriteLine("Press any key to stop the simulators...");
                Console.ReadKey();
                vus.stop();
            }
            else
            {
                using (var c = new UpsClient(o.Host, o.Port))
                {
                    string helperStr = @"
**********************************MENU**********************************
** q : Quit                                                           **
** g : Get UPS list                                                   **
** ga : Get active UPS list                                           **
** gv=<serial> : UPS values belonging to <serial>                     **
** h : Help show                                                      **
************************************************************************
                    ";

                    System.Console.WriteLine(helperStr);
                    for (int i = 0; ; i++)
                    {
                        System.Console.Write(i + ". Input: ");
                        var read = Console.ReadLine();
                        if (read.Equals("q") || read.Equals("Q"))
                            break;

                        if (read.Equals("h"))
                        {
                            System.Console.WriteLine(helperStr);
                            continue;
                        }

                        if (read.Equals("g"))
                        {
                            var list = Task.Run(() => c.GetUpsListAsync());
                            System.Console.WriteLine("List count: " + list.Result.UpsList.Count);
                            continue;
                        }

                        if (read.Equals("ga"))
                        {
                            var activeList = Task.Run(() => c.GetActiveUpsListAsync());
                            System.Console.WriteLine("List count: " + activeList.Result.UpsList.Count);
                            continue;
                        }

                        if (read.Equals("gv"))
                        {
                            var s = read.Split("=");
                            if (s.Length != 2)
                            {
                                System.Console.WriteLine("Not empty serial...");
                                System.Console.WriteLine("Sample use: $gv=123456789-2");
                                continue;
                            }

                            var vals = Task.Run(() => c.GetUpsValuesAsync(s[1]));
                            System.Console.WriteLine("List count: " + vals.Result.ValuesList.Count);
                        }
                    }
                }
            }
        }
    }
}
